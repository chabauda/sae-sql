DELETE FROM prier;
DELETE FROM achat;
DELETE FROM fournir;
DELETE FROM stock;
DELETE FROM staff;
DELETE FROM clients;
DELETE FROM produits;
DELETE FROM boutique;
DELETE FROM fidele;
DELETE FROM categorie;
DELETE FROM temple;
DELETE FROM humain;
DELETE FROM fournisseur;
DELETE FROM lieu;

DROP TABLE prier;
DROP TABLE achat;
DROP TABLE fournir;
DROP TABLE stock;
DROP TABLE staff;
DROP TABLE clients;
DROP TABLE produits;
DROP TABLE boutique;
DROP TABLE fidele;
DROP TABLE categorie;
DROP TABLE temple;
DROP TABLE humain;
DROP TABLE fournisseur;
DROP TABLE lieu;


CREATE TABLE lieu(
   ville VARCHAR(50),
   departement VARCHAR(50) NOT NULL,
   nb_hab INT NOT NULL,
   CONSTRAINT pk_ville PRIMARY KEY(ville)
);

CREATE TABLE fournisseur(
   ID_fournisseur INT,
   nom VARCHAR(50) NOT NULL,
   num_tel INT NOT NULL,
   localisationF VARCHAR(50) NOT NULL,
   CONSTRAINT pk_ID_fournisseur PRIMARY KEY(ID_fournisseur),
   CONSTRAINT fk_localisationF FOREIGN KEY(localisationF) REFERENCES lieu(ville)
);

CREATE TABLE humain(
   ID_humain INT,
   Email VARCHAR(50) NOT NULL,
   nom VARCHAR(50) NOT NULL,
   date_naissance DATE NOT NULL,
   ville_naissance VARCHAR(50) NOT NULL,
   localisationH VARCHAR(50) NOT NULL,
   CONSTRAINT pk_ID_humain PRIMARY KEY(ID_humain),
   CONSTRAINT fk_ville_naissance FOREIGN KEY(ville_naissance) REFERENCES lieu(ville),
   CONSTRAINT fk_localisationH FOREIGN KEY(localisationH) REFERENCES lieu(ville)
);

CREATE TABLE temple(
   ID_temple INT,
   nomT VARCHAR(50) NOT NULL,
   date_creation DATE NOT NULL,
   localisationT VARCHAR(50) NOT NULL,
   CONSTRAINT pk_ID_temple PRIMARY KEY(ID_temple),
   CONSTRAINT fk_localisation FOREIGN KEY(localisationT) REFERENCES lieu(ville)
);

CREATE TABLE categorie(
   nom VARCHAR(50),
   CONSTRAINT pk_nom PRIMARY KEY(nom)
);

CREATE TABLE fidele(
   ID_fidele INT,
   ID_humain INT,
   date_adheration DATE NOT NULL,
   CONSTRAINT pk_ID_fidele PRIMARY KEY(ID_fidele),
   CONSTRAINT fk_ID_humainFid FOREIGN KEY(ID_humain) REFERENCES humain(ID_humain)
);

CREATE TABLE boutique(
   ID_boutique INT,
   nom VARCHAR(50) NOT NULL,
   date_creation DATE NOT NULL,
   localisationB VARCHAR(50) NOT NULL,
   CONSTRAINT pk_ID_boutique PRIMARY KEY(ID_boutique),
   CONSTRAINT fk_localisationB FOREIGN KEY(localisationB) REFERENCES lieu(ville)
);

CREATE TABLE produits(
   ID_produit INT,
   lib VARCHAR(50) NOT NULL,
   prix DECIMAL(15,2) NOT NULL,
   couleur VARCHAR(50),
   nom_categorie VARCHAR(50) NOT NULL,
   CONSTRAINT pk_ID_produit PRIMARY KEY(ID_produit),
   CONSTRAINT fk_nom_categorie FOREIGN KEY(nom_categorie) REFERENCES categorie(nom),
   CONSTRAINT prixPositif CHECK (prix>0),
   CONSTRAINT couleurValide CHECK (couleur IN ('rouge', 'bleu', 'jaune', 'vert', 'orange', 'violet', 'rose', 'noir', 'blanc', 'marron', 'gris'))
);

CREATE TABLE clients(
   ID_client INT,
   point_fidelite INT NOT NULL,
   ID_humain INT NOT NULL,
   CONSTRAINT pk_ID_client PRIMARY KEY(ID_client),
   CONSTRAINT fk_ID_humainC FOREIGN KEY(ID_humain) REFERENCES humain(ID_humain)
);

CREATE TABLE staff(
   ID_staff INT,
   salaire INT NOT NULL,
   ID_temple INT,
   ID_humain INT NOT NULL,
   ID_boutique INT,
   CONSTRAINT pk_ID_staffS PRIMARY KEY(ID_staff),
   CONSTRAINT fk_ID_templeS FOREIGN KEY(ID_temple) REFERENCES temple(ID_temple),
   CONSTRAINT fk_ID_humainS FOREIGN KEY(ID_humain) REFERENCES humain(ID_humain),
   CONSTRAINT fk_ID_boutiqueS FOREIGN KEY(ID_boutique) REFERENCES boutique(ID_boutique),
   CONSTRAINT smic CHECK (salaire>=1353)
);

CREATE TABLE stock(
   ID_boutique INT,
   ID_Produit INT,
   qs INT NOT NULL,
   CONSTRAINT pk_stock PRIMARY KEY(ID_boutique, ID_Produit),
   CONSTRAINT fk_ID_boutiqueSt FOREIGN KEY(ID_boutique) REFERENCES boutique(ID_boutique),
   CONSTRAINT fk_ID_ProduitSt FOREIGN KEY(ID_Produit) REFERENCES produits(ID_Produit),
   CONSTRAINT qsMax CHECK (qs<200),
   CONSTRAINT qsMin CHECK (qs>=0)
);

CREATE TABLE fournir(
   ID_Produit INT,
   ID_fournisseur INT,
   date_livraison DATE NOT NULL,
   CONSTRAINT pk_fournir PRIMARY KEY(ID_Produit, ID_fournisseur),
   CONSTRAINT fk_ID_ProduitFo FOREIGN KEY(ID_Produit) REFERENCES produits(ID_Produit),
   CONSTRAINT fk_ID_fournisseurFo FOREIGN KEY(ID_fournisseur) REFERENCES fournisseur(ID_fournisseur)
);

CREATE TABLE achat(
   ID_Produit INT,
   ID_Client INT,
   qa VARCHAR(50) NOT NULL,
   date_achat DATE NOT NULL,
   CONSTRAINT pk_achat PRIMARY KEY(ID_Produit, ID_Client),
   CONSTRAINT fk_ID_ProduitA FOREIGN KEY(ID_Produit) REFERENCES produits(ID_Produit),
   CONSTRAINT fk_ID_ClientA FOREIGN KEY(ID_Client) REFERENCES clients(ID_Client),
   CONSTRAINT qaMin CHECK (qa>0)
);


CREATE TABLE prier(
   ID_temple INT,
   ID_fidele INT,
   nb_heure INT NOT NULL,
   CONSTRAINT pk_prier PRIMARY KEY(ID_temple, ID_fidele),
   CONSTRAINT fk_ID_templeP FOREIGN KEY(ID_temple) REFERENCES temple(ID_temple),
   CONSTRAINT fk_ID_fideleP FOREIGN KEY(ID_fidele) REFERENCES fidele(ID_fidele)
);






INSERT INTO lieu VALUES ('Sète', 'Hérault', 45000);
INSERT INTO lieu VALUES ('Montpellier', 'Hérault', 280000);
INSERT INTO lieu VALUES ('Nîmes', 'Gard', 150000);
INSERT INTO lieu VALUES ('Paris', 'Île-de-France', 2148000);
INSERT INTO lieu VALUES ('Lyon', 'Auvergne-Rhône-Alpes', 515695);
INSERT INTO lieu VALUES ('Marseille', 'Provence-Alpes-Côte d Azur', 861635);
INSERT INTO lieu VALUES ('Bordeaux', 'Nouvelle-Aquitaine', 255080);
INSERT INTO lieu VALUES ('Lille', 'Hauts-de-France', 232787);
INSERT INTO lieu VALUES ('Strasbourg', 'Grand Est', 280966);
INSERT INTO lieu VALUES ('Nice', 'Provence-Alpes-Côte d Azur', 342439);



INSERT INTO humain VALUES (1, 'elon.musk@email.com', 'Elon Musk', '28.06.1971', 'Sète', 'Sète');
INSERT INTO humain VALUES (2, 'timothee@email.com', 'Timothée Chalamet', '27.12.1995', 'Montpellier', 'Nice');
INSERT INTO humain VALUES (3, 'angelina@email.com', 'Angelina Jolie', '04.06.1975', 'Nîmes', 'Lyon');
INSERT INTO humain VALUES (4, 'brad@email.com', 'Brad Pitt', '18.12.1963', 'Paris', 'Marseille');
INSERT INTO humain VALUES (5, 'oprah@email.com', 'Oprah Winfrey', '29.01.1954', 'Lyon', 'Nîmes');
INSERT INTO humain VALUES (6, 'leonardo@email.com', 'Leonardo DiCaprio', '11.11.1974', 'Marseille', 'Paris');
INSERT INTO humain VALUES (7, 'beyonce@email.com', 'Beyoncé Knowles', '04.09.1981', 'Bordeaux', 'Bordeaux');
INSERT INTO humain VALUES (8, 'barack@email.com', 'Barack Obama', '04.08.1961', 'Lille', 'Strasbourg');
INSERT INTO humain VALUES (9, 'ellen@email.com', 'Ellen DeGeneres', '26.01.1958', 'Strasbourg', 'Lille');
INSERT INTO humain VALUES (10, 'bruno@email.com', 'Bruno Mars', '08.10.1985', 'Nice', 'Montpellier');

INSERT INTO humain VALUES (11, 'bob@email.com', 'Bob', '04.08.1961', 'Sète', 'Sète');
INSERT INTO humain VALUES (12, 'jamy@email.com', 'Jamy', '11.11.1974', 'Sète', 'Sète');
INSERT INTO humain VALUES (13, 'michel@email.com', 'Bob', '08.10.1985', 'Lille', 'Nice');
INSERT INTO humain VALUES (14, 'michel@email.com', 'Bob', '04.06.1975', 'Paris', 'Montpellier');
INSERT INTO humain VALUES (15, 'Julie@email.com', 'Julie', '10.10.10', 'Bordeaux', 'Sète');
INSERT INTO humain VALUES (16, 'Clélia@email.com', 'Clélia', '11.11.11', 'Marseille', 'Lyon');
INSERT INTO humain VALUES (17, 'Jeremy@email.com', 'Jeremy', '08.12.1959', 'Marseille', 'Lyon');
INSERT INTO humain VALUES (18, 'BobyDu34@email.com', 'Boby', '10.02.1999', 'Marseille', 'Lyon');
INSERT INTO humain VALUES (19, 'Roman@email.com', 'Roman Ducros', '25.09.2005', 'Lyon', 'Sète');
INSERT INTO humain VALUES (20, 'Jules@email.com', 'Jule', '25.09.2005', 'Lyon', 'Sète');
INSERT INTO humain VALUES (21, 'Ryan@email.com', 'Ryan', '01.10.2005', 'Lyon', 'Sète');





INSERT INTO temple VALUES (1, 'Temple de la Capybara Divine', '15.01.2022', 'Sète');
INSERT INTO temple VALUES (2, 'Capybara Sanctuaire Sacré', '28.02.2022', 'Montpellier');
INSERT INTO temple VALUES (3, 'Capybara Zen Garden Temple', '10.12.2021', 'Nîmes');
INSERT INTO temple VALUES (4, 'Capybara Temple of Tranquility', '20.03.2022', 'Paris');
INSERT INTO temple VALUES (5, 'Capybara Harmony Haven', '05.11.2021', 'Lyon');
INSERT INTO temple VALUES (6, 'Capybara Blissful Retreat', '12.04.2022', 'Marseille');
INSERT INTO temple VALUES (7, 'Capybara Serenity Shrine', '08.10.2021', 'Bordeaux');
INSERT INTO temple VALUES (8, 'Temple Tranquil du Capybara', '25.05.2022', 'Lille');
INSERT INTO temple VALUES (9, 'Capybara Zenith Ziggurat', '15.09.2021', 'Strasbourg');
INSERT INTO temple VALUES (10, 'Capybara Sacred Grove', '30.06.2022', 'Nice');




INSERT INTO fidele VALUES (1, 2, '15.01.2022');
INSERT INTO fidele VALUES (2, 5, '28.02.2022');
INSERT INTO fidele VALUES (3, 7, '10.12.2021');
INSERT INTO fidele VALUES (4, 9, '20.12.2012');
INSERT INTO fidele VALUES (5, 12, '06.07.2023');
INSERT INTO fidele VALUES (6, 15, '01.08.2019');
INSERT INTO fidele VALUES (7, 16, '15.02.2018');
INSERT INTO fidele VALUES (8, 19, '25.06.2020');
INSERT INTO fidele VALUES (9, 1, '10.10.2020');
INSERT INTO fidele VALUES (10, 20, '07.09.2017');




INSERT INTO clients VALUES (2, 150, 2);
INSERT INTO clients VALUES (3, 200, 3);
INSERT INTO clients VALUES (4, 50, 4);
INSERT INTO clients VALUES (5, 100, 5);
INSERT INTO clients VALUES (6, 20, 6);
INSERT INTO clients VALUES (7, 1000, 7);
INSERT INTO clients VALUES (8, 400, 8);
INSERT INTO clients VALUES (9, 300, 9);
INSERT INTO clients VALUES (10, 350, 10);
INSERT INTO clients VALUES (11, 60, 11);


INSERT INTO boutique VALUES (1, 'CapyShop', '01.05.2000', 'Sète');
INSERT INTO boutique VALUES (2, 'CapySouvenir', '01.01.1950', 'Montpellier');
INSERT INTO boutique VALUES (3, 'CapyLoveShop', '09.05.2005', 'Nîmes');
INSERT INTO boutique VALUES (4, 'CapybaraInc', '01.01.2020', 'Paris');
INSERT INTO boutique VALUES (5, 'CapyLyon', '01.01.2022', 'Lyon');
INSERT INTO boutique VALUES (6, 'TempleCapyShop', '10.01.1999', 'Marseille');
INSERT INTO boutique VALUES (7, 'CapyBoutique', '01.01.1999', 'Bordeaux');
INSERT INTO boutique VALUES (8, 'CapyShop 2', '01.01.2003', 'Sète');
INSERT INTO boutique VALUES (9, 'CapyStrasBourg', '05.01.2000', 'Strasbourg');
INSERT INTO boutique VALUES (10, 'WWWCAPYWWWwSHop', '20.12.1999', 'Nice');



INSERT INTO staff (ID_staff, salaire, ID_humain, ID_boutique) VALUES (7, 2200, 7, 3);
INSERT INTO staff (ID_staff, salaire, ID_temple, ID_humain) VALUES (8, 1900, 1, 8);
INSERT INTO staff (ID_staff, salaire, ID_humain, ID_boutique) VALUES (9, 2100, 9, 5);
INSERT INTO staff (ID_staff, salaire, ID_temple, ID_humain) VALUES (10, 2000, 2, 10);
INSERT INTO staff (ID_staff, salaire, ID_humain, ID_boutique) VALUES (11, 2300, 11, 7);
INSERT INTO staff (ID_staff, salaire, ID_temple, ID_humain) VALUES (12, 1800, 3, 12);
INSERT INTO staff (ID_staff, salaire, ID_humain, ID_boutique) VALUES (13, 1850, 13, 9);
INSERT INTO staff (ID_staff, salaire, ID_temple, ID_humain) VALUES (14, 1950, 4, 14);
INSERT INTO staff (ID_staff, salaire, ID_humain, ID_boutique) VALUES (15, 2500, 15, 10);
INSERT INTO staff (ID_staff, salaire, ID_temple, ID_humain) VALUES (16, 3000, 5, 16);




INSERT INTO categorie VALUES ('Jouet');
INSERT INTO categorie VALUES ('Souvenir');
INSERT INTO categorie VALUES ('Livre');
INSERT INTO categorie VALUES ('Jeux de société');
INSERT INTO categorie VALUES ('Musique');
INSERT INTO categorie VALUES ('Enfants');
INSERT INTO categorie VALUES ('Educatif');
INSERT INTO categorie VALUES ('Adolescents');
INSERT INTO categorie VALUES ('Senior');
INSERT INTO categorie VALUES ('Jeune enfants');




INSERT INTO fournisseur VALUES (1,'Dieu', 04-25-36-55-98, 'Sète');
INSERT INTO fournisseur VALUES (2,'Gaston', 04-30-66-99-08, 'Montpellier');
INSERT INTO fournisseur VALUES (3,'Basti', 07-14-15-63-20, 'Nîmes');
INSERT INTO fournisseur VALUES (4,'Diderm', 01-08-06-71-36,'Paris');
INSERT INTO fournisseur VALUES (5,'Marcel', 02-02-03-20-50, 'Lyon');
INSERT INTO fournisseur VALUES (6,'Dimar', 06-70-32-90-91, 'Marseille');
INSERT INTO fournisseur VALUES (7,'Bateau', 06-35-36-37-63, 'Bordeaux');
INSERT INTO fournisseur VALUES (8,'Couto', 01-80-63-54-95, 'Lille');
INSERT INTO fournisseur VALUES (9,'Fermacier', 03-56-95-94-25, 'Strasbourg');
INSERT INTO fournisseur VALUES (10,'Endoulli', 02-60-35-82-95, 'Nice');



INSERT INTO produits VALUES (1, 'peluche', 25, 'rose', 'Jouet');
INSERT INTO produits VALUES (2, 'peluche', 25, 'vert', 'Jouet');
INSERT INTO produits VALUES (3, 'peluche', 25, 'jaune', 'Jouet');
INSERT INTO produits VALUES (4, 'Chandelle', 25, 'rouge', 'Souvenir');
INSERT INTO produits VALUES (5, 'Chandelle', 25, 'blanc', 'Souvenir');
INSERT INTO produits VALUES (6, 'Chandelle', 25, 'violet', 'Souvenir');
INSERT INTO produits (ID_produit, lib, prix, nom_categorie) VALUES (7, 'Encyclopédie', 25, 'Livre');
INSERT INTO produits (ID_produit, lib, prix, nom_categorie) VALUES (8, 'BD', 25, 'Livre');
INSERT INTO produits (ID_produit, lib, prix, nom_categorie) VALUES (9, 'Eau Capybarine', 25,'Souvenir');
INSERT INTO produits (ID_produit, lib, prix, nom_categorie) VALUES (10, 'Carte Postale', 25,'Souvenir');



INSERT INTO achat VALUES (1, 2, 1, '25.10.2022');
INSERT INTO achat VALUES (2, 2, 1, '25.10.2022');
INSERT INTO achat VALUES (3, 3, 10, '01.08.2022');
INSERT INTO achat VALUES (7, 4, 2, '01.08.2022');
INSERT INTO achat VALUES (8, 5, 5, '05.06.2000');
INSERT INTO achat VALUES (10, 6, 1, '05.06.2005');
INSERT INTO achat VALUES (5, 7, 20, '25.10.1999');
INSERT INTO achat VALUES (5, 8, 10, '10.12.1999');
INSERT INTO achat VALUES (8, 9, 10, '01.08.2022');
INSERT INTO achat VALUES (7, 10, 2, '01.08.2022');
INSERT INTO achat VALUES (4, 11, 2, '01.08.2022');



INSERT INTO prier VALUES (1,1,2000);
INSERT INTO prier VALUES (2,2,13000);
INSERT INTO prier VALUES (3,3,550);
INSERT INTO prier VALUES (4,4,120);
INSERT INTO prier VALUES (5,5,500);
INSERT INTO prier VALUES (6,6,600);
INSERT INTO prier VALUES (7,7,1000);
INSERT INTO prier VALUES (8,8,200);
INSERT INTO prier VALUES (9,9,259);
INSERT INTO prier VALUES (10,10,300);




INSERT INTO fournir VALUES (1,1,'03.12.2022');
INSERT INTO fournir VALUES (2,2,'11.03.2021');
INSERT INTO fournir VALUES (3,3,'09.05.2023');
INSERT INTO fournir VALUES (4,4,'20.10.2022');
INSERT INTO fournir VALUES (5,5,'15.11.2023');
INSERT INTO fournir VALUES (6,6,'25.01.2023');
INSERT INTO fournir VALUES (7,7,'27.06.2023');
INSERT INTO fournir VALUES (8,8,'08.02.2021');
INSERT INTO fournir VALUES (9,9,'05.08.2022');
INSERT INTO fournir VALUES (10,10,'19.12.2022');


INSERT INTO stock VALUES (1,1,20);
INSERT INTO stock VALUES (2,2,150);
INSERT INTO stock VALUES (3,3,100);
INSERT INTO stock VALUES (4,4,60);
INSERT INTO stock VALUES (5,5,90);
INSERT INTO stock VALUES (6,6,120);
INSERT INTO stock VALUES (7,7,160);
INSERT INTO stock VALUES (8,8,10);
INSERT INTO stock VALUES (9,9,80);
INSERT INTO stock VALUES (10,10,70);


INSERT INTO stock VALUES (1,1,20);
INSERT INTO stock VALUES (2,2,150);
INSERT INTO stock VALUES (3,3,100);
INSERT INTO stock VALUES (4,4,60);
INSERT INTO stock VALUES (5,5,90);
INSERT INTO stock VALUES (6,6,120);
INSERT INTO stock VALUES (7,7,160);
INSERT INTO stock VALUES (8,8,10);
INSERT INTO stock VALUES (9,9,80);
INSERT INTO stock VALUES (10,10,70);



--Nom des clients qui ont acheté un produit de couleur rose

SELECT nom FROM humain
WHERE ID_humain in (
   SELECT ID_client FROM clients
   WHERE ID_client IN (
      SELECT ID_client FROM achat
      WHERE ID_produit IN (
         SELECT ID_produit FROM produits
         WHERE couleur = 'rouge'
      )
   )
)

-- NOM : Bob


-- les fournisseur qui ont fournis des peluche rose

SELECT nom FROM fournisseur
WHERE ID_fournisseur IN (
   SELECT ID_fournisseur FROM fournir f
   JOIN produits p ON f.ID_Produit = p.ID_produit
   WHERE lib = 'peluche' AND couleur = 'rose'
)

-- NOM : Dieu


-- les humains qui sont des clients, des fidèles et des membres du staff

SELECT DISTINCT nom FROM humain h
JOIN staff s ON h.ID_humain = s.ID_Humain
JOIN fidele f ON f.ID_Humain = s.ID_Humain
JOIN clients c ON c.ID_Humain = s.ID_Humain

-- le nom et salaire des staff dans l ordre croissant

SELECT DISTINCT nom, salaire FROM humain h
JOIN staff s ON h.ID_humain = s.ID_Humain
ORDER BY salaire


--NOM	SALAIRE
--Jamy 1800
--Bob 1850
--Barack Obama 1900
--Bob	1950
--Bruno Mars 2000
--Ellen DeGeneres	2100
--Beyoncé Knowles	2200
--Bob	2300
--Julie 2500
--Clélia 3000



-- les nom des fideles et de leur temps de prière dans l'ordre décroissant

SELECT nom, SUM(nb_heure) AS temps FROM humain h
JOIN fidele f ON h.ID_humain = f.ID_humain
JOIN prier p ON p.ID_fidele = f.ID_fidele
GROUP BY nom
ORDER BY temps DESC


-- NOM	TEMPS
-- Oprah Winfrey	13000
-- Timothée Chalamet	2000
-- Clélia	1000
-- Julie	600
-- Beyoncé Knowles	550
-- Jamy	500
-- Jule	300
-- Elon Musk	259
-- Roman Ducros	200
-- Ellen DeGeneres	120




6) 
SELECT AVG(salaire) AS Moyenne FROM Staff; <-------- Quelle est la moyenne des salaire du staff?
-- Moyenne: 2160

SELECT MAX(qs) AS Maximum FROM Stock; <---------- Quelle est la quantité maximum qu'à le stock?
-- Maximum: 160

SELECT COUNT(ID_humain)AS Humain FROM humain; <--------- Quels est le nombre d'humain?
-- Humain: 231

7)
SELECT h.ID_humain FROM Humain h <------- Quels sont les humains qui ont acheté des produits de couleur rouge ou rose;
JOIN Clients c ON h.ID_humain= c.ID_humain
JOIN Achat a ON c.ID_Client= a.ID_Client
JOIN Produits p ON a.ID_produit= p.ID_produit
WHERE couleur='rouge'
UNION
SELECT h.ID_humain FROM Humain h
JOIN Clients c ON h.ID_humain= c.ID_humain
JOIN Achat a ON c.ID_Client= a.ID_Client
JOIN Produits p ON a.ID_produit= p.ID_produit
WHERE couleur='rose';
-- ID_Humain: 2
--            11


SELECT nom FROM Humain h < -------- Quel est le nom des fidèles qui n'on jamais prier?
JOIN Fidele f ON h.ID_humain= f.ID_humain
MINUS
SELECT nom FROM Humain h 
JOIN Fidele f ON h.ID_humain= f.ID_humain
JOIN Prier p ON f.ID_fidele= p.ID_fidele;
-- Nom: Ryan


SELECT h.ID_humain FROM Humain h <------- Quels sont les humains qui ont achetés des produits de couleur rouge et rose?
JOIN Clients c ON h.ID_humain= c.ID_humain
JOIN Achat a ON c.ID_Client= a.ID_Client
JOIN Produits p ON a.ID_produit= p.ID_produit
WHERE couleur='rose'
INTERSECT
SELECT h.ID_humain FROM Humain h
JOIN Clients c ON h.ID_humain= c.ID_humain
JOIN Achat a ON c.ID_Client= a.ID_Client
JOIN Produits p ON a.ID_produit= p.ID_produit
WHERE couleur='vert';
-- ID_Humain: 2

8)
SELECT nomT, COUNT(f.ID_fidele) AS NB_fideles, AVG(p.nb_heure) AS Moyenne_Heure_Priere FROM Temple t  <---------- Quelle est le nombre de fidèles dans chaque temple ainsi que la moyenne des heures prier?
JOIN prier p ON t.ID_temple = p.ID_temple
JOIN fidele f ON p.ID_fidele = f.ID_fidele
GROUP BY nomT;



SELECT nom, SUM(p.prix * a.qa) AS total_ventes, COUNT(a.ID_Produit) AS nombre_achats FROM boutique b <-------- Quel la total des ventes par boutique ainsi que le nombre d'achats?
JOIN achat a ON b.ID_boutique = a.ID_Produit
JOIN produits p ON a.ID_Produit = p.ID_produit
GROUP BY nom;


SELECT b.nom, c.nom AS Nom_categorie, COUNT(a.ID_Produit) AS nombre_achats FROM achat a < --------- Combien d'achats ont été éffectués pour les boutiques et les catégories?
JOIN produits p ON a.ID_Produit = p.ID_produit
JOIN boutique b ON a.ID_Produit = b.ID_boutique
JOIN categorie c ON p.nom_categorie = c.nom
GROUP BY b.nom, c.nom;
